

import java.util.ArrayList;

public class ContactDAO implements IContactDAO{

	private ArrayList<Contact> list_contact;
	
	public ContactDAO() {
		this.list_contact = new ArrayList<Contact>();
	}

	public ArrayList<Contact> getListContact() {
		return list_contact;
	}

	public boolean isContactExist(String name) {	
		return list_contact
		 	.stream()
		 	.anyMatch(x->x.getNom().trim().equalsIgnoreCase(name.trim()));
	}
	
	@Override
	public void add(Contact contact) {
		// TODO Auto-generated method stub
		list_contact.add(contact);
	}

	@Override
	public void delete(String name) {
		// TODO Auto-generated method stub
		list_contact
		.removeIf(x->x.getNom().trim().equalsIgnoreCase(name.trim()));
	}

	@Override
	public void update(String name, String newName) {
		// TODO Auto-generated method stub
		list_contact
		.stream()
		.filter(x->x.getNom().trim().equalsIgnoreCase(name.trim()))
		.findFirst()
		.get().setNom(newName);
	}
	
}
