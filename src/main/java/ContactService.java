

public class ContactService implements IContactService{
	
	IContactDAO dao = new ContactDAO();
	
	
	public Contact creerContact(String nom) throws ContactException {
		nom = nom.trim();
		
		if (nom == null || nom.length() < 3 || nom.length() > 40)
			throw new ContactException();
		
		if (!nom.matches("[a-zA-Z]+\\.?"))
			throw new ContactException();
		
		if (dao.isContactExist(nom)) {
			throw new ContactException();
		}
		
		Contact contact = new Contact(nom);
		dao.add(contact);
		
		return contact;
	}
	
	public void supprimerContact(String nom) throws ContactException {
		if (!dao.isContactExist(nom))
			throw new ContactException();
		
		dao.delete(nom);
	}
	
	public void modifierContact(String nom, String nvNom) throws ContactException {
		if(dao.isContactExist(nvNom) || !dao.isContactExist(nom))
			throw new ContactException();
		
		dao.update(nom, nvNom);
	}
	
}
