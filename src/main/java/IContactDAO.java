
public interface IContactDAO {
	boolean isContactExist(String name);
	void add(Contact contact);
	void delete(String name);
	void update(String name, String newName);
}
