

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ContactServiceMockTest {
	
	@Mock
	private IContactDAO contactDao;
	@InjectMocks
	private ContactService contactService = new ContactService();
	
	@Captor
	private ArgumentCaptor<Contact> contactCaptor;
	
	@Test
	public void shouldFailOnDuplicateEntry() throws ContactException {
		// Définition du mock
		Mockito.when(contactDao.isContactExist("Thierry")).thenReturn(true);
		
		// Test
		Assertions.assertThrows(ContactException.class, ()-> contactService.creerContact("Thierry"));
	}
	
	@Test
	public void shouldPass() throws ContactException {
		// Définition du mock
		Mockito.when(contactDao.isContactExist("Thierry")).thenReturn(false);
		
		// Test
		contactService.creerContact("   Thierry   ");
		
		Mockito.verify(contactDao).add(contactCaptor.capture());
		// voir ctn de l'arg
		Contact contact = contactCaptor.getValue();
		Assertions.assertEquals("Thierry", contact.getNom());
		
	}
	
	@Test
	public void shouldFailIfDeleteNothing() throws ContactException {
		// Définition du mock
		Mockito.when(contactDao.isContactExist("Thierry")).thenReturn(false);
		
		// Test
		Assertions.assertThrows(ContactException.class, ()-> contactService.supprimerContact("Thierry"));
	}
	
	@Test
	public void shouldFailIfNewNameIsAlreadyTaken() throws ContactException {
		// Définition du mock
		Mockito.when(contactDao.isContactExist("AncienThierry")).thenReturn(false);
		Mockito.when(contactDao.isContactExist("Thierry")).thenReturn(false);
		
		// Test
		Assertions.assertThrows(ContactException.class, ()-> contactService.modifierContact("AncienThierry", "Thierry"));
	}
}
