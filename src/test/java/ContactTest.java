

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class ContactTest {
	IContactService service = new ContactService();

	@Test
	void shouldFailIfNull() {
		assertThrows(ContactException.class, 
				() -> service.creerContact(null), ContactException.CONTACT_FORMAT);
	}
	
	@Test
	void shouldFailIfLengthIsWrong() {
		assertThrows(ContactException.class, 
				() -> service.creerContact("ab"), ContactException.CONTACT_FORMAT);

		assertThrows(ContactException.class, 
				() -> service.creerContact("azertyuiopqsdfghjklmwxcvbnazertyuiopqsdfg")
				, ContactException.CONTACT_FORMAT); // length of 41 
	}
	
	@Test
	void shouldFailIfVoid() {
		assertThrows(ContactException.class, 
				() -> service.creerContact("   "), ContactException.CONTACT_FORMAT);
	}
	
	@Test
	void shouldFailIfNotLetter() {
		assertThrows(ContactException.class, 
				() -> service.creerContact("12345"), ContactException.CONTACT_FORMAT);
		

		assertThrows(ContactException.class, 
				() -> service.creerContact("$$02"), ContactException.CONTACT_FORMAT);
	}
	
	@Test
	void okIfRightLength() throws ContactException {
		Contact contact = service.creerContact("abc");
		assertTrue(contact.getNom() == "abc");
		
		contact = service.creerContact("abcdabcdabcdabcdabcdabcdabcdabcdabcdabcd"); // length of 40
		assertTrue(contact.getNom() == "abcdabcdabcdabcdabcdabcdabcdabcdabcdabcd");
	}
	
	@Test
	void shouldFailIfDuplicate() throws ContactException {
		service.creerContact("thierry");
		assertThrows(ContactException.class,
				() -> service.creerContact("thierry"), ContactException.CONTACT_DUPLICATION);
		
	}

}
